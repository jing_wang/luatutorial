//
//  main.cpp
//  LuaGameTutorial
//
//  Created by Jing Wang on 4/25/15.
//  Copyright (c) 2015 Jing Wang. All rights reserved.
//

#include <iostream>

#ifdef __cplusplus
extern "C" {
#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
}
#endif

class CGameData
{
public:
    CGameData( int i ) { m_iLockpickLevel = i; }
    int m_iLockpickLevel;
};

char *szLua =
"function game_door_check( context ) "
"  local x = GetLockpickLevel( context ) "
"  return ( x > 7 ) "
"end ";

int lua_GetLockpickLevel( lua_State *luaState )
{
    int nArgs = lua_gettop( luaState );
    if( nArgs != 1 )
    {
        lua_pushstring( luaState, "Script Error 1" );
        lua_error( luaState );
    }
    CGameData *pData = (CGameData *) lua_touserdata( luaState, 1 );
    if( !pData )
    {
        lua_pushstring( luaState, "Script Error 2" );
        lua_error( luaState );
    }
    lua_pushinteger( luaState, pData->m_iLockpickLevel );
    return( 1 );
}


int main(int argc, const char * argv[]) {
    lua_State *lState;
    
    lState = luaL_newstate();
    luaL_openlibs( lState );
    
    lua_register( lState, "GetLockpickLevel", lua_GetLockpickLevel );
    
    int iStatus = luaL_loadstring( lState, szLua );
    if( iStatus )
    {
        std::cout << "Error: " << lua_tostring( lState, -1 );
        return 1;
    }
    
    iStatus = lua_pcall( lState, 0, 0, 0 );
    if( iStatus )
    {
        std::cout << "Error: " << lua_tostring( lState, -1 );
        return 1;
    }
    
    lua_getfield( lState, LUA_GLOBALSINDEX, "game_door_check" );
    CGameData Data( 8 );
    lua_pushlightuserdata( lState, (void *) &Data );
    iStatus = lua_pcall( lState, 1, 1, 0 );
    if( iStatus )
    {
        std::cout << "Error: " << lua_tostring( lState, -1 );
        return 1;
    }
    
    int iRet = (int) lua_toboolean( lState, -1 );
    if( iRet )
    {
        std::cout << "Door opened!" << std::endl;
    }
    else
    {
        std::cout << "Door still closed." << std::endl;
    }
    
    lua_close( lState );
    
    return 0;
}
